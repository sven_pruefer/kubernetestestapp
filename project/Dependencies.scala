/* Copyright (C) 2021 Sven Prüfer - All Rights Reserved
 *
 * This file is part of kubernetestestapp.
 *
 * kubernetestestapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kubernetestestapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kubernetestestapp.  If not, see <https://www.gnu.org/licenses/>.
 */

import sbt._

object Dependencies {

  private object Versions {
    val akka = "2.6.8"

    val `akka-http` = "10.2.3"
  }

  val `akka-typed`   = "com.typesafe.akka" %% "akka-actor-typed" % Versions.akka
  val `akka-streams` = "com.typesafe.akka" %% "akka-stream"      % Versions.akka
  val `akka-http`    = "com.typesafe.akka" %% "akka-http"        % Versions.`akka-http`
}
