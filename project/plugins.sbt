// Package compiled code
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.8.0")

// Set version from GIT
addSbtPlugin("com.dwijnand" % "sbt-dynver" % "4.1.1")
