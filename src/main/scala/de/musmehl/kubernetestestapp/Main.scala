/* Copyright (C) 2021 Sven Prüfer - All Rights Reserved
 *
 * This file is part of kubernetestestapp.
 *
 * kubernetestestapp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kubernetestestapp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kubernetestestapp.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.musmehl.kubernetestestapp

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.util.Random

object Main extends App {

  implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "my-system")
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = system.executionContext

  var current = 0

  val route = {
    concat(
      path("random-number") {
        get {
          val result = Random.between(0, 100)
          println(s"Produced random number $result")
          complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, s"Your random number: $result"))
        }
      },
      path("consecutive-number") {
        get {
          current = current + 1
          println(s"Produced next number $current")
          complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, s"Your consecutive number: $current"))
        }
      },
      path("exit") {
        get {
          system.terminate()
          complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, "Shutting down"))
        }
      }
    )
  }

  val bindingFuture = Http()
    .newServerAt("0.0.0.0", 8080)
    .bind(route)
    .map(_.addToCoordinatedShutdown(hardTerminationDeadline = 10.seconds))

  println(
  """Server online at http://0.0.0.0:8080/
     |Send GET request to http://0.0.0.0:8080/exit to stop...""".stripMargin
  )

  Await.result(system.whenTerminated, Duration.Inf)
}
