import java.net.URL

import Dependencies._

lazy val kubernetestestapp = project
  .in(file("."))
  .settings(
    inThisBuild(
      List(
        organization := "de.musmehl",
        licenses += ("GPL-3.0", new URL("https://www.gnu.org/licenses/gpl-3.0.en.html")),
        scalaVersion := "2.13.4",
        dynverSeparator := "-"
      )
    )
  )
  .settings(
    name := "kubernetestestapp",
    libraryDependencies ++= List(
      `akka-typed`,
      `akka-streams`,
      `akka-http`
    )
  )
  // Build a release
  .enablePlugins(JavaAppPackaging, DockerPlugin)
  .settings(
    maintainer := "sven@musmehl.de",
    dockerBaseImage := "openjdk:11-jre-slim",
    dockerExposedPorts := Seq(8080),
    dockerRepository := Some("registry.gitlab.com"),
    dockerUsername := Some("sven_pruefer")
  )
